package com.sunanda.rampup.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

// 1. What do these to attributes do.
@Slf4j
@RestController
public class CatFactsController {

  private final RestTemplate restTemplate;
  private final ObjectMapper objectMapper;
  private static final String url = "https://catfact.ninja/fact";

  // 2. Where are restTemplate and objectMapper coming from?
  public CatFactsController(RestTemplate restTemplate, ObjectMapper objectMapper) {
    this.restTemplate = restTemplate;
    this.objectMapper = objectMapper;
  }

  // 3. How would we call the endpoint in postman?
  @GetMapping("catfacts")
  public JsonNode GetCatFact() {

    JsonNode jsonNode = null;

    try {
      ResponseEntity<String> response
          = restTemplate.getForEntity(url, String.class);

      logResponse(response);

      jsonNode = objectMapper.readTree(response.getBody());

    } catch (JsonProcessingException jsonProcessingException) {
      log.error("Unable to parse response", jsonProcessingException);
    } catch (Exception e) {
      log.error("Unexpect error", e);
    }

    return jsonNode;
  }

  // 3. Where is the log class coming from?
  private void logResponse(ResponseEntity<String> response) {
    log.info("Response Status: " + response.getStatusCode());
    log.info(response.getBody());
  }
}
